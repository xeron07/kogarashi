const express=require("express");
const bodyParser= require("body-parser");
const mongoose=require("mongoose");
const key=require("./config/key");
const app=express();

//CONFIGURATIONS
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/ext",express.static("ext"));

app.set("view engine","ejs");

mongoose.connect(key.dbString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  app.use("/api/room");

  app.use("/reg",(req,res)=>{
    res.render("registration/reg");
});

app.use("/",(req,res)=>{
    res.render("index");
});




app.listen(4000,()=>{
    console.log(`server started at port no 4000`);
});