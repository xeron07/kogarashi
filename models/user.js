const {Schema} =require("mongoose");

const userSchema=new Schema({
    uname:String,
    password:String,
    role:String,
    access:{type:Boolean,default:false}
});

module.exports=mongoose.modle("users",userSchema);